// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var pertama1 = pertama.substr(0, 5)
var pertama2 = pertama.substr(12, 7)
var prt = pertama1.concat(pertama2)

var kedua1 = kedua.substr(0, 7)
var kedua2 = kedua.substr(7, 11)
var kedua2up = kedua2.toUpperCase()
var kda = kedua1.concat(kedua2up)

var hasil = prt.concat(kda)
console.log(hasil);


// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var bil1 = parseInt(kataPertama)
var bil2 = parseInt(kataKedua)
var bil3 = parseInt(kataKetiga)
var bil4 = parseInt(kataKeempat)

var hasil = bil2 * bil3 + bil1 + bil4
console.log(hasil);


// soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substr(0,3)
var kataKedua = kalimat.substr(4,10)
var kataKetiga = kalimat.substr(15,3)
var kataKeempat = kalimat.substr(19,5)
var kataKelima = kalimat.substr(25,6)

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
